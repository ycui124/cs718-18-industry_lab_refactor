package ictgradschool.industry.lab_refactor.ex01;

import java.io.*;
import java.util.*;

/**
 * TODO Please test & refactor this - my eyes are watering just looking at it :'(
 */
public class ExcelNew {
    String output = "";
    final int CLASS_SIZE = 550;

    public static void main(String[] args) {
        ExcelNew e = new ExcelNew();
        e.start();
    }

    private void start() {
        List<String> firstnameList = readFile("FirstNames.txt");
        List<String> surnameList = readFile("Surnames.txt");
        for (int i = 1; i <= CLASS_SIZE; i++) {
            String student = "";
            student = num(i);
            int randFNIndex = (int) (Math.random() * firstnameList.size());
            int randSNIndex = (int) (Math.random() * surnameList.size());
            student += "\t" + surnameList.get(randSNIndex) + "\t" + firstnameList.get(randFNIndex) + "\t";
            //Student Skill
            int randStudentSkill = (int) (Math.random() * 101);
            //Labs//////////////////////////
            student += labResults(randStudentSkill);
            //Test/////////////////////////
            student += testResults(randStudentSkill);
            ///////////////Exam////////////
            student += examResults(randStudentSkill);
            output += student;
        }
        saveInFile(output);
    }

    private void saveInFile(String output) {
        try (BufferedWriter bw = new BufferedWriter(new FileWriter("Data_Out.txt"))) {
            bw.write(output);
        } catch (IOException e) {
            e.printStackTrace();
        }
    }

    private String examResults(int randStudentSkill) {
        String student = "";
        if (randStudentSkill <= 7) {
            int randDNSProb = (int) (Math.random() * 101);
            if (randDNSProb <= 5) {
                student += ""; //DNS
            } else {
                student += (int) (Math.random() * 40); //[0,39]
            }
        } else if ((randStudentSkill > 7) && (randStudentSkill <= 20)) {
            student += ((int) (Math.random() * 10) + 40); //[40,49]
        } else if ((randStudentSkill > 20) && (randStudentSkill <= 60)) {
            student += ((int) (Math.random() * 20) + 50);//[50,69]
        } else if ((randStudentSkill > 60) && (randStudentSkill <= 90)) {
            student += ((int) (Math.random() * 20) + 70); //[70,89]
        } else {
            student += ((int) (Math.random() * 11) + 90); //[90,100]
        }
        return student += "\n";
    }

    private String testResults(int randStudentSkill) {
        String student = "";
        if (randStudentSkill <= 5) {
            student += (int) (Math.random() * 40); //[0,39]
        } else if ((randStudentSkill > 5) && (randStudentSkill <= 20)) {
            student += ((int) (Math.random() * 10) + 40); //[40,49]
        } else if ((randStudentSkill > 20) && (randStudentSkill <= 65)) {
            student += ((int) (Math.random() * 20) + 50); //[50,69]
        } else if ((randStudentSkill > 65) && (randStudentSkill <= 90)) {
            student += ((int) (Math.random() * 20) + 70); //[70,89]
        } else {
            student += ((int) (Math.random() * 11) + 90); //[90,100]
        }
        return student += "\t";
    }

    private String labResults(int randStudentSkill) {
        String student = "";
        for (int i = 0; i < 3; i++) {
            if (randStudentSkill <= 5) {
                student += (int) (Math.random() * 40); //[0,39]
            } else if ((randStudentSkill > 5) && (randStudentSkill <= 15)) {
                student += ((int) (Math.random() * 10) + 40); // [40,49]
            } else if ((randStudentSkill > 15) && (randStudentSkill <= 25)) {
                student += ((int) (Math.random() * 20) + 50); // [50,69]
            } else if ((randStudentSkill > 25) && (randStudentSkill <= 65)) {
                student += ((int) (Math.random() * 20) + 70); // [70,89]
            } else {
                student += ((int) (Math.random() * 11) + 90); //[90,100]
            }
            student += "\t";
        }
        return student;
    }

    private String num(int i) {
        String student = "";
        if (i / 10 < 1) {
            student += "000" + i;
        } else if (i / 100 < 1) {
            student += "00" + i;
        } else if (i / 1000 < 1) {
            student += "0" + i;
        } else {
            student += i;
        }
        return student;
    }

    private List<String> readFile(String filename) {
        List<String> list = new ArrayList<>();
        try (BufferedReader br = new BufferedReader(new FileReader(filename))) {
            String line;
            while ((line = br.readLine()) != null) {
                list.add(line);
            }
        } catch (FileNotFoundException e) {
            e.printStackTrace();
        } catch (IOException e) {
            e.printStackTrace();
        }
        return list;
    }


}
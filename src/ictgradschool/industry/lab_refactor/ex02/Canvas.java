package ictgradschool.industry.lab_refactor.ex02;

import javax.swing.*;
import java.awt.*;
import java.util.ArrayList;

public class Canvas extends JPanel {

    private int fx = -1, fy = -1;
    private ArrayList<Integer> xs1 = new ArrayList<>();
    private ArrayList<Integer> xs2 = new ArrayList<>();
    private ArrayList<Integer> ys1 = new ArrayList<>();
    private ArrayList<Integer> ys2 = new ArrayList<>();
    private boolean done = false;

    public int getFx() {
        return fx;
    }

    public void setFx(int fx) {
        this.fx = fx;
    }

    public int getFy() {
        return fy;
    }

    public void setFy(int fy) {
        this.fy = fy;
    }

    public ArrayList<Integer> getXs1() {
        return xs1;
    }

    public void setXs1(ArrayList<Integer> xs1) {
        this.xs1 = xs1;
    }

    public ArrayList<Integer> getXs2() {
        return xs2;
    }

    public void setXs2(ArrayList<Integer> xs2) {
        this.xs2 = xs2;
    }

    public ArrayList<Integer> getYs1() {
        return ys1;
    }

    public void setYs1(ArrayList<Integer> ys1) {
        this.ys1 = ys1;
    }

    public ArrayList<Integer> getYs2() {
        return ys2;
    }

    public void setYs2(ArrayList<Integer> ys2) {
        this.ys2 = ys2;
    }

    public boolean isDone() {
        return done;
    }

    public void setDone(boolean done) {
        this.done = done;
    }

    @Override
    public void paintComponent(Graphics g) {
        super.paintComponent(g);
        for (int i = 0; i < xs2.size(); i++) {
            g.setColor(Color.gray);
            g.fill3DRect(xs2.get(i) * 25 + 1, ys2.get(i) * 25 + 1, 25 - 2, 25 - 2, true);
        }
        g.setColor(Color.green);
        g.fill3DRect(fx * 25 + 1, fy * 25 + 1, 25 - 2, 25 - 2, true);
        for (int i = 0; i < xs1.size(); i++) {
            g.setColor(Color.red);
            g.fill3DRect(xs1.get(i) * 25 + 1, ys1.get(i) * 25 + 1, 25 - 2, 25 - 2, true);
        }
        if (done) {
            g.setColor(Color.red);
            g.setFont(new Font("Arial", Font.BOLD, 60));
            FontMetrics fm = g.getFontMetrics();
            g.drawString("Over", (30 * 25 + 6 - fm.stringWidth("Over")) / 2, (20 * 25 + 28) / 2);
        }
    }
}
